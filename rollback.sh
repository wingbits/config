#! /bin/bash
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root to set up the service correctly"
    echo "Run it like this:"
    echo "sudo ./rollback.sh"
    exit 1
fi


echo "Stopping wingbits service..."
systemctl stop wingbits

echo "Disabling wingbits service..."
systemctl disable wingbits

echo "Enabling vector service..."
systemctl enable vector

echo "Starting vector service..."
systemctl start vector

echo "Rollback complete"
systemctl status vector