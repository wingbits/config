# wb-config

## Description
wb-config - a command line GUI configuration tool for general adsb items and Wingbits specific items, with automatic uploads of logs to termbin.com if netcat-traditional is installed (can be done via menu options).

## Installation
```bash
curl -sL https://gitlab.com/wingbits/config/-/raw/master/wb-config/install.sh | sudo bash
```

## Usage
Install script should download and install wb-config and report success or failure.

Once installed, you can run the tool via:
```bash
sudo wb-config
```

## License
MIT License, check LICENSE file at root of repo
