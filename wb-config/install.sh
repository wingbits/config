#!/bin/bash

# Ensure the script is being run as root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root." 
   exit 1
fi

# Define the URL and the destination
URL="https://gitlab.com/wingbits/config/-/raw/master/wb-config/wb-config"
DESTINATION="/usr/local/bin/wb-config"

# Download the file and overwrite if it already exists
curl -o "$DESTINATION" -L --silent --fail "$URL"

# Check if the download was successful
if [[ $? -ne 0 ]]; then
   echo "Failed to download the Wingbits Configuration Tool."
   exit 1
fi

# Make the script executable
chmod 755 "$DESTINATION"

# Refresh the shell's path cache
hash -r

# Inform the user
echo ""
echo "The Wingbits Configuration Tool has been downloaded and is now available to run via:"
echo "sudo wb-config"
echo ""
